import Logger from '../../utils/Logger';
import Comment from '../models/CommentModel';
import Movie from '../models/MovieModel';
import mongoose from 'mongoose';

const controller = {
	errorLogger: new Logger( 'error', 'error.log' ),
	info: new Logger( 'info' ),

	addComment( req, res ) {
		const movie = mongoose.Types.ObjectId( req.body.movie ); //eslint-disable-line
		const comment = new Comment( { movie, comment: req.body.comment } );
		comment.save().then( ( com ) => {
			res.send( com );
			return Movie.findById( movie );
		} ).then( ( foundMovie ) => {
			foundMovie.comments.push( comment._id );
			return foundMovie.save();
		} ).catch( ( err ) => {
			res.send( err );
		} );
	},

	getComments( req, res ) {
		Comment.find( {} ).
			then( ( comments ) => {
				res.send( comments );
			} ).
			catch( ( err ) => {
				res.send( err );
			} );
	},

	getCommentsByMovieId( req, res ) {
		Comment.find( { 'movie': req.params.movie } ).
			then( ( comments ) => {
				res.send( comments );
			} ).
			catch( ( err ) => {
				res.send( err );
			} );
	}

};

export default controller;
