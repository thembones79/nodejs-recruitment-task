import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;
const movieSchema = new Schema( {
	title: {
		type: String,
		required: true
	},
	details: {
		type: Object,
		required: true
	},
	comments: [ {
		type: Schema.Types.ObjectId,
		ref: 'Comments',
		autopopulate: true
	} ]
} );
movieSchema.plugin( autopopulate );

export default mongoose.model( 'Movies', movieSchema );
