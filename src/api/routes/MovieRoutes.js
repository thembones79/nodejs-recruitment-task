import express from 'express';
import controller from '../controllers/MovieController';

const router =  express.Router(); // eslint-disable-line new-cap

router.post( '/', controller.addMovie );
router.get( '/', controller.fetchMovies );

export default router;
