import express from 'express';
import compression from 'compression';
import moviesRouter from './api/routes/MovieRoutes';
import commentsRouter from './api/routes/CommentRoutes';
import morgan from 'morgan';
import cors from 'cors';

const app = express();
app.use( cors() );
app.use( compression() );
app.use( morgan( 'dev' ) );
app.use( express.json() );
app.use( express.urlencoded( { extended: false } ) );
app.use( '/movies', moviesRouter );
app.use( '/comments', commentsRouter );
export default app;
