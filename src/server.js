
import db from './db';
import app from './app';

const port = process.env.PORT || 3000;
db.connect();
app.listen( port, () => {
	console.log( 'App started' );// eslint-disable-line
} );
