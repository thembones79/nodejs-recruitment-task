import { capitalizeFirstLetter } from './StringUtils';

/**
 * createMoviesQuery - creating database query from query string
 *
 * @param  {Object} queries query string params
 * @return {Object}					database query object
 */
export function createMoviesQuery( queries ) {
	const dbQuery = {};
	if ( Object.keys( queries ).length !== 0 && queries.constructor === Object ) {
		for ( const param in queries ) {
			dbQuery[ `details.${capitalizeFirstLetter( param )}` ] = new RegExp ( `.*${queries[ param ]}.*`, 'i' );
		}
	}
	return dbQuery;
}
