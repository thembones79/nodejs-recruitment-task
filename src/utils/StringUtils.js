
/**
 * capitalizeFirstLetter - return string wih first letter uppercase and rest lowercase
 *
 * @param  {string} text given text to capitalize
 * @return {string}      string into the proper format
 */
export function capitalizeFirstLetter( text ) {
	return text.charAt( 0 ).toUpperCase() + text.slice( 1 ).toLowerCase();
}
