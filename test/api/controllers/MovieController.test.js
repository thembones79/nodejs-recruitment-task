const expect = require( 'chai' ).expect;
const mongoose = require( 'mongoose' );
const controller = require( '../../../src/api/controllers/MovieController' ).default;

describe( 'MovieController', () => {

	before( ( done ) => {
		mongoose.connect( `mongodb://${process.env.MONGO_ADDRESS}/${process.env.MONGO_NAME}` );
		const db = mongoose.connection;
		db.on( 'error', () => {} );
		db.once( 'open', () => {
			console.log( 'We are connected to test database!' ); //  eslint-disable-line
			done();
		} );
	} );

	after( ( done ) => {
		mongoose.connection.db.dropDatabase( () => {
			mongoose.connection.close( );
			done();
		} );
	} );

	it( 'return object', () => {
		expect( controller ).to.be.an( 'object' );
	} );

	describe( 'fetchMovieDetails', () => {

		let movieDetails;

		beforeEach( () => {
			movieDetails = controller.fetchMovieDetails( 'sherlock' );
		} );

		it( 'return a promise', ( ) => {
			expect( movieDetails ).to.be.a( 'Promise' );
		} );

		it( 'give response with movie data', ( done ) => {
			movieDetails.then( ( res ) => {
				expect( res.data ).to.be.an( 'Object' );
				done();
			} ).catch( () => {
				done();
			} );
		} );

	} );

} );
