const supertest = require( 'supertest' );
const expect = require( 'chai' ).expect;
const mongoose = require( 'mongoose' );
const router = require( '../src/app' ).default;

describe( 'loading express app', () => {
	const server = router.listen( 3020 );
	const request = supertest.agent( server );

	before( ( done ) => {
		mongoose.connect( `mongodb://${process.env.MONGO_ADDRESS}/${process.env.MONGO_NAME}` );
		const db = mongoose.connection;
		db.on( 'error', () => {} );
		db.once( 'open', () => {
			done();
		} );
	} );

	after( ( done ) => {
		mongoose.connection.db.dropDatabase( () => {
			mongoose.connection.close( );
			server.close( done );
		} );

	} );

	it( 'server exists', ( ) => {
		expect( server ).to.exist;
	} );

	it( 'should give 404 response from GET /', () => {
		return request.get( '/' ).
			expect( 404  );
	} );

	describe( 'POST /movies', () => {

		it( 'gives 200 when title is given', ( done ) => {
			supertest( router ).
				post( '/movies' ).
				send( { title: 'Sherlock' } ).
				expect( 200 ).
				end( ( err ) => {
					if ( err ) {
						return done( err );
					}
					done();
				} );
		} );

		it( 'gives 422 when title is not diven', ( done ) => {
			supertest( router ).
				post( '/movies' ).
				send( {} ).
				expect( 422 ).
				end( ( err ) => {
					if ( err ) {
						return done( err );
					}
					done();
				} );
		} );

		it( 'gives response with title property', ( done ) => {
			supertest( router ).
				post( '/movies' ).
				send( { title: 'Sherlock' } ).
				expect( ( res ) => {
					expect( res.body ).to.have.property( 'title' );
				} ).
				end( ( err ) => {
					if ( err ) {
						return done( err );
					}
					done();
				} );
		} );

		it( 'gives response with details property', ( done ) => {
			supertest( router ).
				post( '/movies' ).
				send( { title: 'Sherlock' } ).
				expect( ( res ) => {
					expect( res.body ).to.have.property( 'details' );
				} ).
				end( ( err ) => {
					if ( err ) {
						return done( err );
					}
					done();
				} );
		} );

		it( 'details property is not empty Object', ( done ) => {
			supertest( router ).
				post( '/movies' ).
				send( { _id: '5b2a6ecb3f16939c5bce2be3', title: 'Sherlock' } ).
				expect( ( res ) => {
					expect( res.body.details ).to.not.deep.equal( {} );
				} ).
				end( ( err ) => {
					if ( err ) {
						return done( err );
					}
					done();
				} );
		} );

	} );

	describe( 'GET /movies', () => {

		it( 'gives 200 response code', ( done ) => {
			supertest( router ).
				get( '/movies' ).
				expect( 200, done );
		} );

		it( 'gives array as an response', ( done ) => {
			supertest( router ).
				get( '/movies' ).
				expect( 200 ).
				end( ( err, res ) => {
					expect( res.body ).to.be.an( 'array' );
					done();
				} );
		} );

		it( 'gives empty array when searching title does not exists in the database', ( done ) => {
			supertest( router ).
				get( '/movies' ).
				query( { title: 'Godfather' } ).
				expect( 200 ).
				end( ( err, res ) => {
					expect( res.body ).to.be.empty;
					done();
				} );
		} );

		it( 'does not gives empty array when searching title exists in the database', ( done ) => {
			supertest( router ).
				get( '/movies' ).
				query( { title: 'Sherlock' } ).
				expect( 200 ).
				end( ( err, res ) => {
					expect( res.body ).to.not.be.empty;
					done();
				} );
		} );

	} );

	describe( 'POST /comment', () => {

		it( 'gives comment object as a response', ( done ) => {
			supertest( router ).
				post( '/comments' ).
				send( { movie: '5b2a6ecb3f16939c5bce2be3', comment: 'This one' } ).
				expect( 200 ).
				end( ( err, res ) => {
					if ( err ) {
						done( err );
					}
					expect( res.body ).to.be.an( 'Object' );
					done();
				} );
		} );

		it( 'has comment and movie property', ( done ) => {
			supertest( router ).
				post( '/comments' ).
				send( { movie: '5b2a6ecb3f16939c5bce2be3', comment: 'This one' } ).
				expect( 200 ).
				expect( ( res ) => {
					expect( res.body ).to.have.property( 'comment' );
					expect( res.body ).to.have.property( 'movie' );
				} ).
				end( ( err ) => {
					if ( err ) {
						done( err );
					}
					done();
				} );
		} );

		describe( ' GET /comments', ( ) => {

			it( 'responds with an array', ( done ) => {
				supertest( router ).
					get( '/comments' ).
					expect( 200 ).
					end( ( err, res ) => {
						expect( res.body ).to.be.an( 'array' );
						expect( res.body ).not.to.be.empty;
						done();
					} );
			} );

		} );
	} );
} );
